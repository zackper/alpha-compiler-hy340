#ifndef QUAD_H
#define QUAD_H

#include "expr_vector.h"

extern int scope;
extern int yylineno;
ST_Entry *id_handler(char* id, char* rule, int entry_scope, int type, int line);

//~~~~~~~~~~~ STRUCTURES
//~~~~ expr

enum expr_t
{
    var_e,
    tableitem_e,

    programfunc_e,
    libraryfunc_e,

    arithexpr_e,
    boolexpr_e,
    newtable_e,

    constnum_e,
    constbool_e,
    constring_e,

    nil_e
};
typedef enum expr_t expr_t;

struct expr
{
    expr_t type;
    ST_Entry* sym;
    struct expr* index;
    long int numConst;
    char* strConst;
    unsigned int boolConst;
    struct expr* next; // just to make trivial s-lists ???
};
typedef struct expr  expr;

//~~~~ quad

#define EXPAND_SIZE 1024
#define CURR_SIZE (total*sizeof(quad))
#define NEW_SIZE (EXPAND_SIZE*sizeof(quad)+ CURR_SIZE)

char *op_translate[] = {"ASSIGN", "ADD", "SUB", "MUL", "DIV", "MOD", "UMINUS", "AND", "OR", "NOT", "IF_EQ", "IF_NOTEQ",
                        "IF_LESSEQ", "IF_GREATEREQ", "IF_LESS", "IF_GREATER", "CALL", "PARAM", "RET", "GETRETVAL", "FUNCSTART",
                        "FUNCEND", "TABLECREATE", "TABLEGETELEM", "TABLESETELEM"};

enum iopcode
{
    assign,     add,            sub,
    mul,        divide,         mod,
    uminus,     and,            or,
    not,        if_eq,          if_noteq,
    if_lesseq,  if_greatereq,   if_less,
    if_greater, call,           param,
    ret,        getretval,      funcstart,
    funcend,    tablecreate,    tablegetelem,
    tablesetelem
};

struct quad
{
    enum iopcode op;
    expr* result;
    expr* arg1;
    expr* arg2;
    unsigned label;
    unsigned line;
};
typedef struct quad quad;

quad* quads = (quad*) 0;
unsigned total = 0;
unsigned int currQuad = 0;

void expand(void)
{
    assert(total == currQuad);
    quad* p = (quad*)malloc(NEW_SIZE);
    if(quads)
    {
        memcpy(p,quads, CURR_SIZE);
    }
    quads = p;
    total += EXPAND_SIZE;
}

void patchlabel(unsigned quadNo, unsigned label)
{
    assert(quadNo < currQuad);
    quads[quadNo].label = label;
}

unsigned nextquadlabel(void)
{
    return currQuad;
}

struct callsuffix_s {
        expr_vector *elist;
        char        *name;
        int         method;
};
typedef struct callsuffix_s callsuffix_s;

//~~~~~~~~~~~
//~~~~~~~~~~~ For printing quads

char* op2string(enum iopcode code){
    return op_translate[code];
}

/**
 * Epeidh mporei na einai int h char* sto quad, eftiaksa auth
 * thn function wste na epistrfw to ti tha tupwthei ston endiameso kwdika.
 */
char* expr_value(expr *e)
{
    assert(e);
    if(e->type == constnum_e)
    {
        char *constnum_str = malloc(sizeof(char)*12);
        sprintf(constnum_str, "%d", e->numConst);
        int len = strlen(constnum_str);
        constnum_str[len] = '\0';
        return constnum_str;
    }
    else if(e->type == constring_e)
    {
        return e->strConst;
    }
    else{
        return get_entry_name(e->sym);
    }
}

void quads_print()
{
    int i;
    enum iopcode current_op;
    char *result, *arg1, *arg2;
    for(i = 0; i < currQuad; i++){
        current_op = quads[i].op;
        if(current_op == add
            || current_op == sub
            || current_op == mul
            || current_op == divide
            || current_op == mod
        )
        {
            result = expr_value(quads[i].result);
            arg1 = expr_value(quads[i].arg1);
            arg2 = expr_value(quads[i].arg2);
            printf("%d: %s %s %s %s\n", quads[i].label, op2string(current_op), result, arg1, arg2);
        }
        else if(current_op == assign)
        {
            result = expr_value(quads[i].result);
            arg1 = expr_value(quads[i].arg1);
            printf("%d: %s %s %s\n", quads[i].label, op2string(current_op), result, arg1);
        }
        else if(current_op == funcstart || current_op == funcend)
        {
            result = expr_value(quads[i].result);
            printf("%d: %s %s\n", quads[i].label, op2string(current_op), result);
        }
        else if(current_op == tablecreate)
        {
            result = expr_value(quads[i].result);
            printf("%d: %s %s\n", quads[i].label, op2string(current_op), result);
        }
        else if(current_op == tablesetelem)
        {
            result = expr_value(quads[i].result);
            arg1 = expr_value(quads[i].arg1);
            arg2 = expr_value(quads[i].arg2);
            printf("%d: %s %s %s %s\n", quads[i].label, op2string(current_op), result, arg1, arg2);
        }
        else if(current_op == tablegetelem)
        {
            result = expr_value(quads[i].result);
            arg1 = expr_value(quads[i].arg1);
            arg2 = expr_value(quads[i].arg2);
            printf("%d: %s %s %s %s\n", quads[i].label, op2string(current_op), arg2, result, arg1);
        }
        else if(current_op == call)
        {
            result = expr_value(quads[i].result);
            printf("%d: %s %s\n", quads[i].label, op2string(current_op), result);
        }
        else if(current_op == param)
        {
            result = expr_value(quads[i].result);
            printf("%d: %s %s\n", quads[i].label, op2string(current_op), result);
        }
        else if(current_op == getretval)
        {
            result = expr_value(quads[i].result);
            printf("%d: %s %s\n", quads[i].label, op2string(current_op), result);
        }
        else
            printf("%d: %s\n", quads[i].label, op2string(current_op));
    }
}

//~~~~~~~~~~~
//~~~~~~~~~~~ #EXPR FUNCTIONS

int temp_counter = 0;
ST_Entry *newtemp(){

    char *str_temp_counter = malloc(sizeof(char)*12);
    sprintf(str_temp_counter, "%d", temp_counter);

    int len = strlen(str_temp_counter);
    
    char *temp_id = malloc(sizeof(char) * (len + 2));
    int i;
    for(i = 0; i < len; i++){
        temp_id[i + 1] = str_temp_counter[i];
    }
    temp_id[0] = '_';
    temp_counter++;
    printf("SCOPE: %d\n", scope);
    ST_Entry *temp = id_handler(temp_id, "newtemp() call", scope, 2, -1);

    return temp;
}

void reset_temp_counter()
{
    temp_counter = 0;
}

unsigned int istempname (const char* s) {
	return *s == '_';
}

unsigned int istempexpr (expr* e) {
	return e->sym && istempname(get_entry_name(e->sym)) ;
}

void emit(enum iopcode op,
    expr* result,
    expr* arg1,
    expr* arg2,
    unsigned label,
    unsigned line)
{
    if(currQuad == total)
        expand();

    quad* p = quads + currQuad++;
    p->op = op;
    p->arg1 = arg1;
    p->arg2 = arg2;
    p->result = result;
    p->label = label;
    p->line = line;
}

expr* lvalue_expr (ST_Entry* sym)
{
    assert(sym);
    expr* e = (expr*) malloc(sizeof(expr));
    memset(e, 0, sizeof(expr));

    e->next = (expr*) 0;
    e->sym = sym;

    switch(sym->type)
    {
        case var_s: e->type = var_e; break;
        case  programfunc_s: e->type = programfunc_e; break;
        case libraryfunc_s:  e->type = libraryfunc_e; break;
        default: assert(0);
    }

    return e;
}

expr* newexpr(expr_t t)
{
    expr* e = (expr*)malloc(sizeof(expr));
    memset(e, 0, sizeof(expr));
    e->type = t;
    return e;
}

expr* emit_iftableitem(expr* e)
{
    if(e->type != tableitem_e)
        return e;
    else
    {
        expr* result = newexpr(var_e);
        result->sym = newtemp();
        emit(tablegetelem, e, e->index, result, nextquadlabel(), yylineno);
        return result;
    }
}

expr* newexpr_constring (char* s)
{
    assert(s);
    expr* e = newexpr(constring_e);
    e->strConst = strdup(s);
    return e;
}

expr *newexpr_constnum(int num)
{
    expr* e = newexpr(constnum_e);
    e->numConst = num;
    return e;
}

expr* make_call (expr* lv, expr_vector *reversed_elist) {
    assert(lv);

	expr* func = emit_iftableitem(lv);
    if(reversed_elist != NULL){
        int i;
        for (i = 0; i < reversed_elist->size; i++) {
            emit(param, reversed_elist->array[i], NULL, NULL, nextquadlabel(), yylineno);
        }
    }
	emit(call, func,NULL, NULL, nextquadlabel(), yylineno);
	expr* result = newexpr(var_e);
	result->sym = newtemp();
	emit(getretval, result, NULL, NULL, nextquadlabel(), yylineno);
	return result;
}

expr* member_item (expr* lv, char* name)
{
	lv = emit_iftableitem(lv);  // Emit code if r-value use of table item
	expr* ti  = newexpr(tableitem_e);  // Make a new expression
	ti->sym   = lv->sym;
	ti->index = newexpr_constring(name); // Const string index
	return ti;
}

//~~~~~~~~~~~~ #QUAD FUNCTIONS

struct stmt_t {
    int breakList, contList;
};
typedef struct stmt_t stmt_t;

void make_stmt (stmt_t* s)
{
    s->breakList = s->contList = 0;
}

int newlist (int i)
{
    quads[i].label = 0;
    return i;
}

int mergelist (int l1, int l2) {
    if (!l1)
        return l2;
    else
    if (!l2)
        return l1;
    else {
        int i = l1;
        while (quads[i].label)
            i = quads[i].label;
        quads[i].label = l2;
        return l1;
    }
}

void patchlist (int list, int label) {
	while (list) {
       int next = quads[list].label;
       quads[list].label = label;
	   list = next;
  }
}


#endif