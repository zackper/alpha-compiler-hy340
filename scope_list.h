/* PHASE 1 
* Myrsini Gkolemi 3929
* Zacharias Pervolarakis 4027 
*/


#include "symbol_table.h"
#include <math.h>
#include <stdlib.h>


#define SCALE_FACTOR 2
#define INIT_SIZE 2 // size 3

struct Scope_Lists
{
    int size;
    ST_Entry** scope_d_array;

};

typedef struct Scope_Lists Scope_Lists;

Scope_Lists* scope_lists;

Scope_Lists* init_scope_lists()
{
    Scope_Lists* new_list;
    int i;
    new_list = (Scope_Lists*)malloc(sizeof(Scope_Lists));
    new_list->scope_d_array =  (ST_Entry**)malloc(sizeof(ST_Entry*)*(INIT_SIZE+1));
    new_list->size = INIT_SIZE;
    for(i = 0 ; i <= INIT_SIZE; i++)
        new_list->scope_d_array[i] = NULL;
    return new_list;
}

void scale_factor_scope_lists(Scope_Lists* scope_lists)
{
    assert(scope_lists);
    assert(scope_lists->scope_d_array);
    int i;
    int new_size;
    new_size = scope_lists->size * (int)ceil(SCALE_FACTOR);
    scope_lists->scope_d_array = realloc(scope_lists->scope_d_array,scope_lists->size * (int)ceil(SCALE_FACTOR)*sizeof(ST_Entry*));
    for( i = scope_lists->size; i <= new_size+1 ; i++)
        scope_lists->scope_d_array[i] = NULL;
    scope_lists->size = new_size;
}

void scale_scope_lists(Scope_Lists* scope_lists, int scope)
{
    assert(scope_lists);
    assert(scope_lists->scope_d_array);
    int i;
    scope_lists->scope_d_array = (ST_Entry**)realloc(scope_lists->scope_d_array,(scope+1)*sizeof(ST_Entry*));
    for( i = scope_lists->size + 1; i <=scope; i++)
        scope_lists->scope_d_array[i] = NULL;
    scope_lists->size = scope;
}

void insert_scope_lists(Scope_Lists* scope_lists,ST_Entry* scope_node) {
    assert(scope_lists);
    assert(scope_lists->scope_d_array);
    assert(scope_node);
    ST_Entry *temp_entry;

    int entry_scope;
    entry_scope = get_entry_scope(scope_node);

    if (entry_scope > scope_lists->size)
    {
            scale_scope_lists(scope_lists, entry_scope);
    }
    printf("ENTRY SCOPE: %d\n", entry_scope);
    if (scope_lists->scope_d_array[entry_scope] == NULL){
        scope_lists->scope_d_array[entry_scope] = scope_node;
    }
    else
    {
        temp_entry = scope_lists->scope_d_array[entry_scope];
        scope_node->scope_next = temp_entry->scope_next;
        temp_entry->scope_next = scope_node;
    }
}

void print_token(ST_Entry* token)
{
    assert(token);
    assert(token->value.var || token->value.func);
    printf("Is active: %d\t", token->isActive);
    if(is_var(token))
        printf("\"%s\" [%s] (line %d) (scope %d)\n",token->value.var->name, symbol_t_map[token->type],
                                                    token->value.var->line,token->value.var->scope);
    else
        printf("\"%s\" [%s] (line %d) (scope %d)\n",token->value.func->name, symbol_t_map[token->type],
                                                    token->value.func->line,token->value.func->scope);
}

void print_list(ST_Entry* list)
{
    if(list == NULL)
        return;
    print_list(list->scope_next); //recursive
    print_token(list);
}

void print_scope_lists(Scope_Lists* scope_lists)
{
    assert(scope_lists);
    assert(scope_lists->scope_d_array);
    int i;
    for( i = 0 ; i<= scope_lists->size ; i++)
    {
        printf("-----------     Scope     #%d-----------\n",i);
        print_list(scope_lists->scope_d_array[i]);
    }
}


/*
int main()
{
    
    ST_Entry*  first;
    ST_Entry*  second;
    ST_Entry*  third;
    ST_Entry*  fourth;   //same scope as third
    Scope_Lists* sc = init_scope_lists();
    hash_map = create_hash_map(5);

    first = create_var_entry("d", 0, 0, LOCAL_VAR);
    second = create_var_entry("a", 1, 0, GLOBAL_VAR);
    
    insert_scope_lists(sc,first);
    insert_scope_lists(sc,second);
    third = create_var_entry("asa", 9, 0, GLOBAL_VAR);
    insert_scope_lists(sc,third);
    fourth = create_var_entry("die", 4, 0, USER_FUNC);
    insert_scope_lists(sc,fourth);
    fourth = create_var_entry("nop", 2, 0, USER_FUNC);
    insert_scope_lists(sc,fourth);
    fourth = create_var_entry("nop", 2, 0, USER_FUNC);
    insert_scope_lists(sc,fourth);
    fourth = create_var_entry("nety", 1, 0, USER_FUNC);
    insert_scope_lists(sc,fourth);
    fourth = create_var_entry("yea", 1, 0, USER_FUNC);
    insert_scope_lists(sc,fourth);

    //create_var_entry("f", 2, 0, LOCAL);
    //create_var_entry("z", 3, 0, LOCAL);
    //create_var_entry("u", 4, 0, LOCAL);
    //create_var_entry("y", 5, 0, LOCAL);
    //create_var_entry("t", 2, 0, LOCAL);
    //create_var_entry("r", 1, 0, LOCAL);
    //create_var_entry("e", 1, 0, LOCAL);

    print_scope_lists(sc);

    return 0;
}
*/

