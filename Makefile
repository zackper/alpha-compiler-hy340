all:
	clear
	bison --yacc --defines --output=parser.c  parser.y
	flex --outfile=scanner.c scanner.l
	gcc -g  scanner.c parser.c -o parser

bison:
	bison --yacc --defines --output=parser.c parser.y

flex:
	flex --outfile=scanner.c scanner.l

test:
	./parser < testfile.txt

go:
	clear
	bison --yacc --defines --output=parser.c  parser.y
	flex --outfile=scanner.c scanner.l
	gcc -g scanner.c parser.c -o parser
	./parser < testfile.txt

clean:
	rm -f parser
	rm -f parser.c
	rm -f parser.h
	rm -f scanner.c
