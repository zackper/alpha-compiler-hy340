%{
    /* PHASE 2
    * Myrsini Gkolemi 3929
    * Zacharias Pervolarakis 4027
    */

    /*Prologue (macros,function declarations,variable declarations)*/
    #include <stdio.h>
    #include <stdlib.h>
    #include "st_handler.h"
    #include "int_stack.h"

    extern int yylex();
    extern int yyerror(const char* something);
    int is_library_function(char* function_name);

    int scope = 0;

    extern int yylineno;
    extern char* yytext;
    extern FILE* yyin;

    int_stack *function_scope;
    int_stack *loop_scope;
	int_stack *space_scope;
%}

%error-verbose
%token PLUS PLUS_PLUS MINUS UMINUS MINUS_MINUS PRODUCT OBELUS MOD GREATER GREATER_EQUAL LESS LESS_EQUAL EQUAL NOT_EQUAL
%token SEMICOLON COLON	DOUBLE_COLON CURLY_BRACKET_L CURLY_BRACKET_R BRACKET_L BRACKET_R
%token PARENTHESIS_L PARENTHESIS_R DOT DOUBLE_DOT COMA
%token AND OR NOT TRUE FALSE
%token <strVal> ID
%token <strVal> STRING
%token <intVal> NUMBER
%token NIL
%token BREAK CONTINUE RETURN
%token LOCAL FUNCTION
%token IF ELSE FOR WHILE

%union
{
    int intVal;
    char *strVal;
    int boolVal;
    unsigned unisgnedVal;
    struct ST_Entry *stVal;
    struct expr *exprVal;
    struct expr_vector *exprVector;
    struct callsuffix_s *callsuffiVal;
}

%type <exprVal> lvalue;
%type <strVal> funcname;
%type <exprVal> tableitem;
%type <exprVal> expr;
%type <exprVal> term;
%type <exprVal> primary;
%type <exprVal> const;
%type <exprVal> assignexpr;
%type <unisgnedVal> funcbody;
%type <stVal> funcprefix;
%type <stVal> funcdef;
%type <intVal> idlist;
%type <intVal> funcargs;
%type <exprVal> objectdef;
%type <exprVector> elist;
%type <exprVector> indexed;
%type <exprVal> indexedelem;
%type <exprVal> call;
%type <callsuffiVal> callsuffix;
%type <callsuffiVal> normcall;
%type <callsuffiVal> methodcall;

%right  ASSIGN
%right  NOT PLUS_PLUS MINUS_MINUS UMINUS

%left   OR
%left   AND
%left   PLUS MINUS
%left   PRODUCT OBELUS MOD
%left   DOT DOUBLE_DOT
%left   BRACKET_L BRACKET_R
%left   PARENTHESIS_L PARENTHESIS_R

%nonassoc   EQUAL NOT_EQUAL
%nonassoc   GREATER GREATER_EQUAL LESS LESS_EQUAL
%start     program
/*Grammar Description*/
%%

program:    stmts           {printf("program: stmts\n\n");}
            |               {printf("program: e\n");}
            ;

stmts:      stmts stmt      {printf("stmts: stmts stmt\n\n");}
            | stmt          {printf("stmts: stmt\n\n"); }
            ;

stmt:       expr SEMICOLON          {
                                        printf("stmt: expr SEMICOLON\n");
                                        reset_temp_counter();
                                    }
            | ifstmt                { printf("stmt: ifstmt\n"); }
            | whilestmt             { printf("stmt: whilestmt\n"); }
            | forstmt               { printf("stmt: forstmt\n"); }
            | returnstmt            { printf("stmt: returnstmt\n"); }
            | BREAK SEMICOLON       { check_loop_state("stmt: BREAK SEMICOLON", yylineno); printf("stmt: BREAK SEMICOLON\n"); }
            | CONTINUE SEMICOLON    { check_loop_state("stmt: CONTINUE SEMICOLON", yylineno); printf("stmt: CONTINUTE SEMICOLON\n"); }
            | block                 { printf("stmt: block\n"); }
            | funcdef               { printf("stmt: funcdef\n"); }
            | SEMICOLON             { printf("stmt: SEMICOLIN\n"); }
            ;

expr:       assignexpr                  {
                                            printf("expr: assignexpr\n");
                                            $$ = $1;
                                        }
            | expr PLUS expr            {
                                            printf("expr: expr PLUS expr\n");
                                            check_arith($1, "expr PLUS expr", yylineno);
                                            check_arith($3, "expr PLUS expr", yylineno);
                                            expr *e = newexpr(arithexpr_e);
                                            e->sym = newtemp();
                                            emit(add, e, $1, $3, nextquadlabel(), yylineno); //TODO: label and line params;
                                            $$ = e;
                                        }
            | expr MINUS expr           {
                                            printf("expr: expr MINUS expr\n");
                                            check_arith($1, "expr MINUS expr", yylineno);
                                            check_arith($3, "expr MINUS expr", yylineno);
                                            expr *e = newexpr(arithexpr_e);
                                            e->sym = newtemp();
                                            emit(sub, e, $1, $3, nextquadlabel(), yylineno); //TODO: label and line params;
                                            $$ = e;
                                        }
            | expr PRODUCT expr         {
                                            printf("expr: expr PRODUCT expr\n");
                                            check_arith($1, "expr PRODUCT expr", yylineno);
                                            check_arith($3, "expr PRODUCT expr", yylineno);
                                            expr *e = newexpr(arithexpr_e);
                                            e->sym = newtemp();
                                            emit(mul, e, $1, $3, nextquadlabel(), yylineno); //TODO: label and line params;
                                            $$ = e;
                                        }
            | expr OBELUS expr          {
                                            printf("expr: expr OBELUS expr\n");
                                            check_arith($1, "expr OBELUS expr", yylineno);
                                            check_arith($3, "expr OBELUS expr", yylineno);
                                            expr *e = newexpr(arithexpr_e);
                                            e->sym = newtemp();
                                            emit(divide, e, $1, $3, nextquadlabel(), yylineno); //TODO: label and line params;
                                            $$ = e;
                                        }
            | expr MOD expr             {
                                            printf("expr: expr MOD expr\n");
                                            check_arith($1, "expr MOD expr", yylineno);
                                            check_arith($3, "expr MOD expr", yylineno);
                                            expr *e = newexpr(arithexpr_e);
                                            e->sym = newtemp();
                                            emit(mod, e, $1, $3, nextquadlabel(), yylineno); //TODO: label and line params;
                                            $$ = e;
                                        }
            | expr GREATER expr         {
                                            printf("expr: expr GREATER expr\n");
                                            check_bool($1, "expr GREATER expr", yylineno);
                                            check_bool($3, "expr GREATER expr", yylineno);
                                            expr *e = newexpr(boolexpr_e);
                                            e->sym = newtemp();
                                            emit(if_greater, e, $1, $3, nextquadlabel(), yylineno); //TODO: label and line params;
                                            $$ = e;
                                        }
            | expr GREATER_EQUAL expr   {
                                            printf("expr: expr GREATER_EQUAL expr\n");
                                            check_bool($1, "expr GREATER_EQUAL expr", yylineno);
                                            check_bool($3, "expr GREATER_EQUAL expr", yylineno);
                                            expr *e = newexpr(boolexpr_e);
                                            e->sym = newtemp();
                                            emit(if_greatereq, e, $1, $3, nextquadlabel(), yylineno); //TODO: label and line params;
                                            $$ = e;
                                        }
            | expr LESS expr            {
                                            printf("expr: expr LESS expr\n");
                                            check_bool($1, "expr LESS expr", yylineno);
                                            check_bool($3, "expr LESS expr", yylineno);
                                            expr *e = newexpr(boolexpr_e);
                                            e->sym = newtemp();
                                            emit(if_less, e, $1, $3, nextquadlabel(), yylineno); //TODO: label and line params;
                                            $$ = e;
                                        }
            | expr LESS_EQUAL expr      {
                                            printf("expr: expr LESS_EQUAL expr\n");
                                            check_bool($1, "expr LESS_EQUAL expr", yylineno);
                                            check_bool($3, "expr LESS_EQUAL expr", yylineno);
                                            expr *e = newexpr(boolexpr_e);
                                            e->sym = newtemp();
                                            emit(if_lesseq, e, $1, $3, nextquadlabel(), yylineno); //TODO: label and line params;
                                            $$ = e;
                                        }
            | expr EQUAL expr           { //TODO: ADD TABLE COMPARISON WITH NILL
                                            printf("expr: expr EQUAL expr\n");
                                            expr *e = newexpr(boolexpr_e);
                                            e->sym = newtemp();
                                            emit(if_eq, e, $1, $3, nextquadlabel(), yylineno); //TODO: label and line params;
                                            $$ = e;
                                        }
            | expr NOT_EQUAL expr       {
                                            printf("expr: expr NOT_EQUAL expr\n");
                                            expr *e = newexpr(boolexpr_e);
                                            e->sym = newtemp();
                                            emit(if_noteq, e, $1, $3, nextquadlabel(), yylineno); //TODO: label and line params;
                                            $$ = e;
                                        }
            | expr AND expr             {
                                            printf("expr: expr AND expr\n");
                                            expr *e = newexpr(boolexpr_e);
                                            e->sym = newtemp();
                                            emit(and, e, $1, $3, nextquadlabel(), yylineno); //TODO: label and line params;
                                            $$ = e;
                                        }
            | expr OR expr              {//i think NOT is missing. + TODO: make expr to bool for comparison.
                                            printf("expr: expr OR expr\n");
                                            expr *e = newexpr(boolexpr_e);
                                            e->sym = newtemp();
                                            emit(or, e, $1, $3, nextquadlabel(), yylineno); //TODO: label and line params;
                                            $$ = e;
                                        }
            | term                      {
                                            printf("expr: term\n");
                                            $$ = $1;
                                        }
            ;

term:       PARENTHESIS_L expr PARENTHESIS_R    {printf("term: PARENTHESIS_L expr PARENTHESIS_R");}
            | MINUS expr %prec UMINUS           {printf("term: \n");}
            | NOT expr                          {printf("term: NOT expr\n");}
            | PLUS_PLUS lvalue                  {printf("term: PLUS_PLUS lvalue\n");}
            | lvalue PLUS_PLUS                  {printf("term: lvalue PLUS_PLUS\n");}
            | MINUS_MINUS lvalue                {printf("term: MINUS_MINUS lvalue\n");}
            | lvalue MINUS_MINUS                {printf("term: lvalue MINUS_MINUS\n");}
            | primary                           {
                                                    printf("term: primary\n");
                                                    $$ = $1;
                                                }
            ;

assignexpr: lvalue ASSIGN expr  {
                                    printf("assignexpr: lvalue ASSIGN expr\n");

                                    if($1->type == tableitem_e)
                                    {
                                        emit(tablesetelem, $1, $1->index, $3,nextquadlabel(), yylineno);
                                        $$ = emit_iftableitem($1);
                                        $$->type = var_e;
                                    } 
                                    else
                                    {
                                        emit(assign, $1, $3, NULL, nextquadlabel(), yylineno);
                                        
                                        expr *e = newexpr(var_e);
                                        e->sym = newtemp();
                                        emit(assign, e, $1, NULL, nextquadlabel(), yylineno); 
                                        $$ = e;
                                    }
                                }
            ;

primary:    lvalue                                  {
                                                        printf("primary: lvalue\n");
                                                        $$ = emit_iftableitem($1);  
                                                    }
            | call                                  {printf("primary: call\n");}
            | objectdef                             {printf("primary: objectdef\n");}
            | PARENTHESIS_L funcdef PARENTHESIS_R   {printf("primary: PARENTHESIS_L funcdef PARENTHESIS_R\n");}
            | const                                 {
                                                        printf("primary: const\n");
                                                        $$ = $1;
                                                    }
            ;

lvalue:     ID                  {
                                    printf("lvalue: ID | %s\n", $1);
                                    ST_Entry *entry = id_handler($1,"lvalue: ID", scope, STANDARD_TYPE, yylineno);
                                    assert(entry); // Not sure for this assertion.
                                    $$ = newexpr(var_e);
                                    $$->sym = entry;
                                }
            | LOCAL ID          {
                                    printf("lvalue: LOCAL ID | %s\n", $2);
                                    ST_Entry *entry = id_handler($2, "lvalue: LOCAL ID",scope, LOCAL_TYPE, yylineno);
                                    assert(entry);
                                    $$ = newexpr(var_e);
                                    $$->sym = entry;
                                }
            | DOUBLE_COLON ID   {
                                    printf("lvalue: ::ID | %s\n", $2);
                                    ST_Entry *entry = id_handler($2,"lvalue: ::ID",scope, GLOBAL_TYPE, yylineno);
                                    assert(entry);
                                    $$ = newexpr(var_e);
                                    $$->sym = entry;
                                }
            | member            {
                                    printf("lvalue: member\n");
                                    //TODO: NOT SURE WHAT TODO, MIRTO HELP.
                                }
            ;

tableitem:  lvalue DOT ID                       {
                                                    printf("tableitem: lvalue DOT ID\n");
                                                    $$ = member_item($1, $3);
                                                }
            | lvalue BRACKET_L expr BRACKET_R   {
                                                    printf("tableitem: lvalue BRACKET_L expr BRACKET_R\n");
                                                    $1 = emit_iftableitem($1);
                                                    $$ = newexpr(tableitem_e);
                                                    $$->sym = $1->sym;
                                                    $$->index = $3;
                                                }
member:     tableitem                           {printf("member: tableitem\n");}
            | call DOT ID                       {printf("member: call DOT ID\n");}
            | call BRACKET_L expr BRACKET_R     {printf("member: call BRACKET_L expr BRACKET_R\n");}
            ;

call:       call normcall           {
                                        printf("call: call PARENTHESIS_L elist PARENTHESIS_R\n");
                                        $$ = make_call($1, $2->elist);
                                    }
            | lvalue callsuffix     {
                                        printf("call: lvalue callsuffix\n");
                                        $1 = emit_iftableitem($1);

                                        if($2->method == 1){
                                            expr *t = $1;    
                                            $1 = emit_iftableitem(member_item(t, $2->name));
                                            expr_vector_add($2->elist, t);
                                        }
                                        $$ = make_call($1, $2->elist);
                                        
                                    }
            | PARENTHESIS_L funcdef PARENTHESIS_R PARENTHESIS_L elist PARENTHESIS_R 
                                    {
                                        printf("call: PARENTHESIS_L funcdef PARENTHESIS_R PARENTHESIS_L elist PARENTHESIS_R\n");
                                        expr *f = newexpr(programfunc_e);
                                        f->sym = $2;
                                        $$ = make_call(f, $5);
                                    }
            | PARENTHESIS_L funcdef PARENTHESIS_R PARENTHESIS_L PARENTHESIS_R       
                                    {
                                        printf("call: PARENTHESIS_L funcdef PARENTHESIS_R PARENTHESIS_L PARENTHESIS_R\n");
                                    }
            ;

callsuffix: normcall        {
                                printf("callsufix: normcall\n");
                                $$ = $1;
                            }
            | methodcall    {
                                printf("callsufix: methodcall\n");
                                $$ = $1;
                            }
            ;

normcall:   PARENTHESIS_L elist PARENTHESIS_R   {
                                                    printf("normcall: PARENTHESIS_L elist PARENTHESIS_R\n");
                                                    $$ = (callsuffix_s *)malloc(sizeof(callsuffix_s));
                                                    $$->method = 0;
                                                    $$->elist = $2;
                                                    $$->name = NULL;
                                                }
            | PARENTHESIS_L  PARENTHESIS_R      {
                                                    printf("normcall: PARENTHESIS_L PARENTHESIS_R\n");
                                                    $$ = (callsuffix_s *)malloc(sizeof(callsuffix_s));
                                                    $$->elist = NULL;
                                                    $$->name = NULL;
                                                    $$->method = 0;
                                                }
            ;

methodcall: DOUBLE_DOT ID PARENTHESIS_L elist PARENTHESIS_R {
                                                                printf("methodcall: DOUBLE_DOT ID PARENTHESIS_L elist PARENTHESIS_R\n");
                                                                $$ = (callsuffix_s *)malloc(sizeof(callsuffix_s));
                                                                $$->method = 1;
                                                                $$->name = $2;
                                                                $$->elist = $4;
                                                            }
            | DOUBLE_DOT ID PARENTHESIS_L PARENTHESIS_R     {
                                                                printf("methodcall: DOUBLE_DOT ID PARENTHESIS_L elist PARENTHESIS_R\n");
                                                                $$ = (callsuffix_s *)malloc(sizeof(callsuffix_s));
                                                                $$->method = 1;
                                                                $$->name = $2;
                                                                $$->elist = expr_vector_init(); //Init because later
                                                                                                //caller needs to be added to vector
                                                            }
            ;

elist:      expr                { 
                                    printf("elist: expr\n"); 
                                    $$ = expr_vector_init();
                                    expr_vector_add($$, $1);
                                }
            | elist COMA expr   {
                                    printf("elist: elist COMA expr\n");
                                    assert($1);
                                    expr_vector_add($1, $3);
                                    printf("WOWWW: %s\n", expr_value(expr_vector_at($$, $1->size - 1)));
                                }
            ;

objectdef:  BRACKET_L elist BRACKET_R       {
                                                printf("objectdef: BRACKET_L elist BRACKET_R\n");

                                                int i;
                                                expr* t = newexpr(newtable_e);
                                                t->sym = newtemp(); 
                                                emit(tablecreate, t, NULL, NULL, nextquadlabel(), yylineno); 

                                                expr_vector *vector = $2;
                                                for(i = 0; i < vector->size; i++)
                                                {
                                                    emit(tablesetelem, t, newexpr_constnum(i), expr_vector_at(vector, i), nextquadlabel(), yylineno);
                                                }
                                                $$ = t;
                                            }
            | BRACKET_L indexed BRACKET_R   {
                                                printf("objectdef: BRACKET_L indexed BRACKET_R\n");
                                                
                                                int i;
                                                expr* t = newexpr(newtable_e);
                                                t->sym = newtemp(); 
                                                emit(tablecreate, t, NULL, NULL, nextquadlabel(), yylineno); 

                                                expr_vector *vector = $2;
                                                expr *index, *value;
                                                for(i = 0; i < vector->size; i++)
                                                {
                                                    index = expr_vector_at(vector, i);
                                                    value = index->next;
                                                    emit(tablesetelem, t, index, value, nextquadlabel(), yylineno);
                                                }
                                                $$ = t;
                                            }
            | BRACKET_L BRACKET_R           {
                                                printf("objectdef: BRACKET_L BRACKET_R\n"); 
                                                expr* t = newexpr(newtable_e);
                                                t->sym = newtemp(); 
                                                emit(tablecreate, t, NULL, NULL, nextquadlabel(), yylineno); 
                                                $$ = t;
                                            }
            ;

indexed:    indexedelem                 {
                                            printf("indexed: indexedelem\n");
                                            $$ = expr_vector_init();
                                            expr_vector_add($$, $1);
                                        }
            | indexed COMA indexedelem  {
                                            printf("indexed: indexed COMA indexedelem\n");
                                            assert($1 && $3);
                                            expr_vector_add($1, $3);
                                        }
            ;

indexedelem: CURLY_BRACKET_L expr COLON expr CURLY_BRACKET_R
                                        {
                                            printf("Indexedelem: CURLY_BRACKET_L expr COLON expr CURLY_BRACKET_R\n");

                                            expr *index = $2; 
                                            expr *value = $4;
                                            index->next = value;
                                            $$ = index;
                                        }
            ;
block_left:     CURLY_BRACKET_L {
                                    printf("block_left: CURLY_BRACKET_L\n");
                                    scope++;
                                }
                ;

block_right:    CURLY_BRACKET_R {
                                    printf("block_right: CURLY_BRACKET_R\n");
                                    hide_scope(scope);
                                    scope--;
                                }

block:      block_left block_right { printf("block: block_left block_right\n"); }
            | block_left stmts block_right { printf("block: block_left stmts block_right\n"); }
            ;

funcname:   ID  {
                    printf("funcname: ID\n");
                    $$ = $1; 
                   
                }
            |   {
                    printf("funcname: NO ID\n");
                    $$ = NULL;
                }
            ;

funcprefix: FUNCTION funcname   {
                                    printf("funcprefix: FUNCTION funcname\n");
                                    scope++;
                                    $$ = id_handler($2,"funcdef: FUNCTION PARENTHESIS_L idlist PARENTHESIS_R block",
                                                    scope, FUNCDEF_TYPE, yylineno);
                                    expr* e = newexpr(programfunc_e);
                                    e->sym = $$;
                                    emit(funcstart, e, NULL, NULL, nextquadlabel(), yylineno);
                                    
                                    stack_push(function_scope, scope);
									pushscopespace(space_scope, currscopeoffset()); /*nested functions*/
                                    printf("PUSHED LOCAL OFFSET\n");
									enterscopespace();
									resetformalargsoffset();
                                }
            ;

funcargs:   PARENTHESIS_L idlist PARENTHESIS_R  {
                                                    $$ = $2;                    /*formal arguments number*/
													enterscopespace();
													resetfunctionlocaloffset(); /*local variables from zero*/
                                                    scope--;                                       
                                                }
            ;

funcbody:   block   {
						$$ = currscopeoffset(); 
                        exitscopespace();
                    }
            ;

funcdef:    funcprefix funcargs funcbody    {       
												exitscopespace();
												($1)->value.func->n_locals = $3;
                                                ($1)->value.func->n_args = $2;
                                                
                                                printf("funcdef: funcprefix funcargs funcbody\n");
												int offset = popscopespace(space_scope);
                                                printf("%s: LOCAL VARIABLES OFFSET: %d , FORMAL_ARGS OFFSET: %d\n",get_entry_name($1),$3,$2);
												restorecurrscopeoffset(offset);
												$$ = $1;
                                                stack_pop(function_scope);

                                                expr *e = newexpr(programfunc_e);
                                                e->sym = $1;
                                                emit(funcend, e, NULL, NULL, nextquadlabel(), yylineno);
                                            }
            ;



const:      NUMBER      {  
                            printf("const: NUMBER (%d)\n", $1);
                            int x = $1;
                            expr *e = newexpr_constnum(x);
                            printf("numConst: %d\n", e->numConst);
                            $$ = e;
                        }
            | STRING    {
                            printf("const: STRING (%s)\n", $1);
                            expr *e = newexpr(constring_e);
                            e->strConst = $1;
                            $$ = e;
                        }
            | NIL       {
                            printf("const: NIL\n");
                            expr *e = newexpr(nil_e);
                            $$ = e;
                        }
            | TRUE      {
                            printf("const: TRUE\n");
                            expr *e = newexpr(constbool_e);
                            e->boolConst = 1;
                            $$ = e;
                        }
            | FALSE     {
                            printf("const: FALSE\n");
                            expr *e = newexpr(constbool_e);
                            e->boolConst = 0;
                            $$ = e;
                        }
            ;

idlist:     ID                  {
                                    printf("IdList: ID: %s\n", $1);
                                    id_handler($1,"Idlist: idlist ID",scope, FORMAL_TYPE,yylineno);
                                    increment_formalArgOffset(); $$ = currscopeoffset();
                                }
            | idlist COMA ID    {
                                    printf("Idlist: idlist , ID:%s\n", $3);
                                    id_handler($3,"Idlist: idlist ID: ",scope, FORMAL_TYPE,yylineno);
                                    increment_formalArgOffset();
                                    $$ = currscopeoffset();
                                }
            |                   { 
                                    printf("Idlist: empty\n"); 
                                }
            ;

ifstmt:     IF PARENTHESIS_L expr PARENTHESIS_R stmt            {printf("ifstmt: IF PARENTHESIS expr PARENTHESIS stmt\n");}
            |IF PARENTHESIS_L expr PARENTHESIS_R stmt ELSE stmt {printf("ifstmt: IF PARENTHESIS_L expr PARENTHESIS_R stmt ELSE stmt\n");}
            ;

whilestmt:  WHILE PARENTHESIS_L expr PARENTHESIS_R {stack_push(loop_scope, scope);} stmt {
                                                                        printf("whilestmt: WHILE PARENTHESIS_L expr PARENTHESIS_R stmt\n");
                                                                        stack_pop(loop_scope);
                                                                    }
            ;

forstmt:    FOR PARENTHESIS_L elist SEMICOLON expr SEMICOLON elist PARENTHESIS_R {stack_push(loop_scope, scope);} 
                                                                            stmt        {
                                                                                            printf("forstmt: FOR PARENTHESIS_L ELIST SEMICOLON expr SEMICOLON elist PARENTHESIS_R stmt\n");
                                                                                            stack_pop(loop_scope);
                                                                                        }
            |FOR PARENTHESIS_L SEMICOLON expr SEMICOLON elist PARENTHESIS_R {stack_push(loop_scope, scope);} 
                                                                            stmt        {
                                                                                            printf("forstmt: FOR PARENTHESIS_L  SEMICOLON expr SEMICOLON elist PARENTHESIS_R stmt\n"); 
                                                                                            stack_pop(loop_scope);
                                                                                        }
            |FOR PARENTHESIS_L elist SEMICOLON expr SEMICOLON PARENTHESIS_R {stack_push(loop_scope, scope);}
                                                                            stmt        {
                                                                                            printf("forstmt: FOR PARENTHESIS_L ELIST SEMICOLON expr SEMICOLON  PARENTHESIS_R stmt\n");
                                                                                            stack_pop(loop_scope);
                                                                                        }
            |FOR PARENTHESIS_L SEMICOLON expr SEMICOLON PARENTHESIS_R {stack_push(loop_scope, scope);}
                                                                            stmt        {
                                                                                            printf("forstmt: FOR PARENTHESIS_L  SEMICOLON expr SEMICOLON  PARENTHESIS_R stmt\n");
                                                                                            stack_pop(loop_scope);
                                                                                        }
            ;

returnstmt: RETURN SEMICOLON            {printf("returnstmt: RETURN SEMICOLON\n"); id_handler(NULL, "returnstmt: RETURN SEMICOLON", scope, RETURN_TYPE, yylineno);}
            | RETURN expr SEMICOLON     {printf("returnstmt: RETURN expr SEMICOLON\n"); id_handler(NULL, "returnstmt: RETURN expr SEMICOLON", scope, RETURN_TYPE, yylineno);} 
            ;

%%

int yyerror(const char* errorMessage)
{
    fprintf (stderr, "%s\n\n", errorMessage);
    return 1;
}


int main(int argc, char **argv)
{
    //~~~~~~~~~~~Initializations~~~~~~~~~~~
    hash_map = create_hash_map(5);
    scope_lists = init_scope_lists();
    insert_libfunc();
    function_scope = stack_init();
    loop_scope = stack_init();
	space_scope = stack_init();
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    yyparse();
    //st_print();
    print_scope_lists(scope_lists);
    printf("\n\n");
    
    quads_print();
    printf("PROGRAM VARS:%d\n",programVarOffset);
    return 0;
}

int a_yyerror(const char* errorMessage,const char* rule, int line)
{
    fprintf(stderr,"\nError at line(%d) rule: (%s) -> ",line,rule);
    yyerror(errorMessage);
}