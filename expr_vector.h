/* PHASE 1 
* Myrsini Gkolemi 3929
* Zacharias Pervolarakis 4027 
*/

#ifndef EXPR_VECTOR_H
#define EXPR_VECTOR_H

#include <stdio.h>
#include <stdlib.h>

typedef struct expr expr;

//~~~~~~~~~~~~~~~~~~~~~~
typedef struct expr_vector {
    expr **array;
    int size;
} expr_vector;

expr_vector *expr_vector_init()
{
    expr_vector *vector = malloc(sizeof(expr_vector));
    vector->array = NULL;
    vector->size = 0;
}

void expr_vector_add(expr_vector *vector, expr *e)
{
    assert(vector);
    assert(e);
    vector->array = realloc(vector->array, (vector->size + 1)*sizeof(expr*));
    vector->size++;

    vector->array[vector->size - 1] = e;
}

expr *expr_vector_at(expr_vector *vector, int index)
{
    assert(vector);
    assert(index >= 0 && index < vector->size);

    return vector->array[index];
}

void expr_vector_clear(expr_vector *vector)
{
    if(vector == NULL)
        return;

    free(vector->array);
    vector->size = 0;
}

#endif