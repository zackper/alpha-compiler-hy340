#ifndef SCOPE_SPACE_H
#define SCOPE_SPACE_H
#include <assert.h>
//~~~~

unsigned programVarOffset = 0;
unsigned functionLocalOffset = 0;
unsigned formalArgOffset = 0;
unsigned scopeSpaceCounter = 1;

enum scopespace_t
{
    programvar,
    functionlocal,
    formalarg
};
typedef enum scopespace_t scopespace_t;

//~~~~

void increment_programVarOffset()
{
    programVarOffset++;
}

void increment_formalArgOffset()
{
    formalArgOffset++;
}
void increment_functionLocalOffset()
{
    functionLocalOffset++;
}

scopespace_t currscopespace(void)
{
    if(scopeSpaceCounter == 1)
        return programvar;
    else if(scopeSpaceCounter % 2 == 0)
        return formalarg;
    else
        return functionlocal;
}

unsigned currscopeoffset()
{
    switch(currscopespace())
    {
	    case programvar:	return programVarOffset;
	    case functionlocal:	return functionLocalOffset; 
	    case formalarg:		return formalArgOffset; 
        default:            assert(0);
    }
}

void resetformalargsoffset(void)
{
    formalArgOffset = 0;
};

void resetfunctionlocaloffset(void)
{
    functionLocalOffset = 0;
};

void restorecurrscopeoffset(unsigned n)
{
    switch (currscopespace())
    {
        case programvar:        programVarOffset = n;       break;
        case functionlocal:     functionLocalOffset = n;    break;
        case formalarg:         formalArgOffset = n;        break;
        default: assert(0);

    }
}

void incurrscopeoffset()
{
	switch(currscopespace())
    {
	    case programvar:	++programVarOffset; 	break;
	    case functionlocal:	++functionLocalOffset;	break;
	    case formalarg:		++formalArgOffset;		break;
	    default:	assert(0);
    }
}

void enterscopespace(void)
{
	++scopeSpaceCounter;	
}

void exitscopespace(void)
{
	assert(scopeSpaceCounter > 1);
	--scopeSpaceCounter;
}

void pushscopespace(int_stack* scope_stack, unsigned current_scope)
{
	stack_push(scope_stack,current_scope);
}

scopespace_t popscopespace(int_stack* scope_stack)
{
	return stack_pop(scope_stack);
}



#endif