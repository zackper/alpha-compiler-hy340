/* PHASE 1
* Myrsini Gkolemi 3929
* Zacharias Pervolarakis 4027
*/

#ifndef SYMBOL_TABLE_H
#define SYMBOL_TABLE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

//*********************************************
extern int scope;

//~~~~~~~~~~~~~~~~
/* HERE LIES THE ALMIGHTY SYMBOL TABLE */
char* symbol_t_map[5] = {"global variable","local variable","formal_variable","user function","lib function"};
enum Symbol_t
{

    GLOBAL_VAR, LOCAL_VAR, FORMAL_VAR,
    USER_FUNC, LIB_FUNC,

    var_s, programfunc_s, libraryfunc_s
};

typedef struct Variable
{
    char      *name;
    unsigned int    scope;
    unsigned int    line;
    unsigned int    space;
    unsigned int    offset;
	
} Variable;

typedef struct Function
{
    char      *name;
    unsigned int    n_locals;
    unsigned int    n_args; //not necessary
    unsigned int    scope;
    unsigned        line;
} Function;

typedef struct ST_Entry
{
    union
    {
        Variable *var;
        Function *func;
    }               value;
    int             isActive;
    enum Symbol_t   type;
    scopespace_t    scope_space;
    struct ST_Entry *st_next;
    struct ST_Entry *scope_next;
} ST_Entry;

//~~~~~~~~~~~~~~~~

#define GET_VAR(entry) entry->value.var
#define GET_FUNC(entry) entry->value.func
#define ST_VAR  0
#define ST_FUNC 1

int is_var(ST_Entry* entry)
{
    assert(entry != NULL);
    enum Symbol_t type = entry->type;
    if(type == LOCAL_VAR || type == GLOBAL_VAR)
        return 1;
    else
        return 0;
}

char *get_entry_name(ST_Entry *entry)
{
    if(is_var(entry))
        return GET_VAR(entry)->name;
    else
        return GET_FUNC(entry)->name;
}

int get_entry_scope(ST_Entry *entry)
{
    if(is_var(entry))
        return GET_VAR(entry)->scope;
    else
        return GET_FUNC(entry)->scope;
}
int get_entry_line(ST_Entry *entry)
{
    if(is_var(entry))
        return GET_VAR(entry)->line;
    else
        return GET_FUNC(entry)->line;
}

ST_Entry *create_var_entry(char *name, int scope, int line, enum Symbol_t type)
{
    assert(name != NULL);

    ST_Entry *entry = (ST_Entry *)malloc(sizeof(ST_Entry));
    entry->isActive = 1;
    entry->type = type;
    entry->st_next = NULL;

    GET_VAR(entry) = (Variable *)malloc(sizeof(Variable));
    GET_VAR(entry)->name = name;
    GET_VAR(entry)->scope = scope;
    GET_VAR(entry)->line = line;

    return entry;
}

ST_Entry *create_func_entry(char *name, int scope, int line, enum Symbol_t type)
{
    assert(name != NULL);

    ST_Entry *entry = (ST_Entry *)malloc(sizeof(ST_Entry));
    entry->isActive = 1;
    entry->type = type;

    GET_FUNC(entry) = (Function *)malloc(sizeof(Function));
    GET_FUNC(entry)->name = name;
    GET_FUNC(entry)->scope = scope;
    GET_FUNC(entry)->line = line;

    return entry;
}

//~~~~~~~~~~~~~~~~

//Tha gnwrizw apo thn grammatikh ean kati einai function h variable.
struct Hash_Item
{
    ST_Entry *entry;
};
typedef struct Hash_Item Hash_Item;

Hash_Item **hash_map; //Global vaiable representing the Symbol Table;
int hash_map_size;

Hash_Item **create_hash_map(int size)
{
    hash_map_size = size;

    assert(hash_map_size > 0);

    int i;
    Hash_Item **hash_map;
    hash_map = (Hash_Item **)malloc(sizeof(Hash_Item *)*hash_map_size);
    for(i = 0; i < hash_map_size; i++){
        hash_map[i] = (Hash_Item *)malloc(sizeof(Hash_Item));
        hash_map[i]->entry = NULL;
    }

    return hash_map;
}



/* THIS IS A SECOND HASH FUNCTION FOR STRINGS I FOUND. LIKED THE SECOND ON BETTER
int hash_function(char *string)
{
    int index;
    int i;
    int string_len = strlen(string);

    unsigned long hash = 5381;
    int c;

    while(c = *string++)
        hash = (hash * 33) + c;

    index = hash;
    printf("Hash: %lu\n", hash);
    return index;
}
*/

//~~~~~~~~~~~~~~~~
// Hash & Index:

unsigned int string2hash(char *key)
{
    assert(key != NULL);

    int i;
    int string_len = strlen(key);

    int g = 31;
    unsigned int hash = 0;
    for(i = 0; i < string_len; i++){
        hash = g * hash + key[i];
    }

    return hash;
}

int hash2index(unsigned int hash)
{
    assert(hash_map_size > 0);
    int index;

    index = hash % hash_map_size;
    if(index >= hash_map_size){
        index = hash2index(index);
    }

    assert(index >= 0 && index < hash_map_size);

    return index;
}

/* Uses string2hash and hash2index to return the INDEX*/
int hash(char *key)
{
    int hash = string2hash(key);
    return hash2index(hash);
}

//~~~~~~~~~~~~~~~~
// General functions:

void st_insert_entry(ST_Entry* new_entry)
{
    assert(hash_map != NULL);
    assert(new_entry != NULL);

    int index;
    char *name;

    if(is_var(new_entry))
        name = GET_VAR(new_entry)->name;
    else //is_func
        name = GET_FUNC(new_entry)->name;

    index = hash(name);

   
    Hash_Item *hash_cell = hash_map[index];
    
    if(hash_cell->entry == NULL ){
        
        new_entry->st_next = NULL;
        hash_cell->entry = new_entry;
        return;
    }
    else{
        /*Sorted insertion*/
        ST_Entry* prev = hash_cell->entry;
        ST_Entry* iterator = hash_cell->entry;
        while(iterator!=NULL && GET_VAR(new_entry)->scope < GET_VAR(iterator)->scope)
        {
            prev = iterator;
            iterator = iterator->st_next;
        }
        if(prev == iterator)
        {
            
            new_entry->st_next = iterator;    // or prev
            hash_cell->entry = new_entry;
        }
        else
        {
          
            prev->st_next = new_entry;
            new_entry->st_next = iterator;
        }
         
        return;
    }

    return;
}





void st_insert_var_entry(char *name, int scope, int line, enum Symbol_t type)
{
    ST_Entry *entry = create_var_entry(name, scope, line, type);
    st_insert_entry(entry);
}

void st_insert_func_entry(char *name, int scope, int line, enum Symbol_t type)
{
    ST_Entry *entry = create_func_entry(name, scope, line, type);
    st_insert_entry(entry);
}

ST_Entry *st_standard_look_up(char *id, int scope)
{
    assert(id != NULL);
    assert(scope >= 0);

    int index = hash(id);

    ST_Entry *iterator = hash_map[index]->entry;
    while(iterator != NULL){
        if(is_var(iterator)){
            if( strcmp(GET_VAR(iterator)->name, id) == 0 && iterator->isActive == 1){
                if(scope >= GET_VAR(iterator)->scope)
                    return iterator;
            }
        }
        else if( strcmp(GET_FUNC(iterator)->name, id) == 0 && iterator->isActive == 1){
                if(scope >= GET_FUNC(iterator)->scope)
                    return iterator;
        }

        iterator = iterator->st_next;
    }

    return NULL;
}

ST_Entry *st_scope_look_up(char *id, int scope)
{
    assert(id != NULL);
    assert(scope >= 0);

    int index = hash(id);

    ST_Entry *iterator = hash_map[index]->entry;
    while(iterator != NULL && iterator->isActive == 1){
        if( strcmp(get_entry_name(iterator), id) == 0 ){
            if(get_entry_scope(iterator) == scope)
                return iterator;
        }

        iterator = iterator->st_next;
    }

    return NULL;
}

void st_print(){
    assert(hash_map);

    int index;
    ST_Entry *iterator;

    printf("-----------     Hash Map     -----------\n");
    for(index = 0; index < hash_map_size; index++){
        printf("Index[%d]:\n", index);
        iterator = hash_map[index]->entry;
        while(iterator != NULL){
            char *name;
            int scope, line;

            if(is_var(iterator)){
                name = GET_VAR(iterator)->name;
                scope = GET_VAR(iterator)->scope;
                line = GET_VAR(iterator)->line;
            }
            else{ //IS_FUNC
                name = GET_FUNC(iterator)->name;
                scope = GET_FUNC(iterator)->scope;
                line = GET_FUNC(iterator)->line;
            }
            printf("\tName: \"%s\", Scope: %d, Line: %d\n", name, scope, line);

            iterator = iterator->st_next;
        }
    }
}




//~~~~~~~~~~~~~~~~

//*********************************************

#endif