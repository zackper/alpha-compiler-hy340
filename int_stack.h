/* PHASE 1 
* Myrsini Gkolemi 3929
* Zacharias Pervolarakis 4027 
*/

#ifndef INT_STACK_H
#define INT_STACK_H

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
//~~~~~~~~~~~~~~~~~~~~~~
typedef struct int_stack {
    int *pointer;
    int size;
    int top;
} int_stack;

int_stack *stack_init(){
    int_stack *new_stack;
    new_stack = (int_stack*)malloc(sizeof(int_stack));
    assert(new_stack);
    new_stack->size = 0;
    new_stack->top = -1;
    return new_stack;
}

void stack_push(int_stack *stack, int element){
    assert(stack);
    
    stack->pointer = (int*)realloc(stack->pointer, sizeof(int)*(stack->size + 1));
    assert(stack->pointer);
    stack->size++;

    stack->pointer[stack->size - 1] = element;
    stack->top = element;
}

int stack_pop(int_stack *stack){
    assert(stack);
    int ret = stack->top;

    stack->pointer = (int*)realloc(stack->pointer, sizeof(int)*(stack->size - 1));
    stack->size--;
    if(stack->size == 0)
        stack->top = -1;
    else
        stack->top = stack->pointer[stack->size - 1];

    return ret;
}

void stack_print(int_stack *stack){
    int i = 0;
    printf("~~~~Stack~~~~\n");
    for(i = 0; i < stack->size; i++){
        printf("[%d]: %d\n", i, stack->pointer[i]);
    }
    printf("\n");
}

int is_empty_stack(int_stack* stack)
{
    return stack->size == 0 ? 1 : 0 ;
}
//~~~~~~~~~~~~~~~~~~~~~~

#endif