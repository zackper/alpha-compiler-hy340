%{
    /* PHASE 1
     * Myrsini Gkolemi 3929
     * Zacharias Pervolarakis 4027
     */

    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include <math.h>
    #include "parser.h"

    extern YYSTYPE yylval;
    extern int yyparse();

    int comment_size = 0;

    int size = 0;
    int counter = 0;
    char* string;
    int* comment;
%}


/* ~~~~~~~~~~~~~~~~~~~~~~~ */
/* REGEX */

/* KEYWORDS */
IF              "if"
ELSE            "else"
WHILE           "while"
FOR             "for"
FUNCTION        "function"
RETURN          "return"
BREAK           "break"
CONTINUE        "continue"
AND             "and"
NOT             "not"
OR              "or"
LOCAL           "local"
TRUE            "true"
FALSE           "false"
NIL             "nil"

/* OPERATORS */
ASSIGN          "="
PLUS            "+"
MINUS           "-"
PRODUCT         "*"
OBELUS          "/"
MOD             "%"
EQUAL           "=="
NOT_EQUAL       "!="
PLUS_PLUS       "++"
MINUS_MINUS     "--"
GREATER         ">"
GREATER_EQUAL   ">="
LESS            "<"
LESS_EQUAL      "<="

/* ARITHMETIC CONST */
INT_CONST       [0-9]{1,10}
REAL_CONST      [0-9]{1,10}\.([0-9]+)

/* ALPHARITHMETIC */
STRING_BEGIN    "\""
STRING_END      "\""
ESC             "\\"
IDENTIFIER      [a-zA-Z]+(_|[0-9]|[a-zA-Z])*

/* PUNCTUATION */
SEMICOLON		";"
COLON			":"
DOUBLE_COLON	"::"
CURLY_BRACKET_L "{"
CURLY_BRACKET_R "}"
BRACKET_L       "["
BRACKET_R       "]"
PARENTHESIS_L   "("
PARENTHESIS_R   ")"
DOT             "."
DOUBLE_DOT      ".."
COMA            ","

SINGLE_COMMENT  "\/\/"
MULTI_COMMENT	"\/\*"
CLOSE_COMMENT   "*/"

NEW_LINE        "\n"
CARRIAGE        "\r\n"
SPACE           " "
TAB             "\t"
OTHER           .

/* ~~~~~~~~~~~~~~~~~~~~~~~ */

%option noyywrap
%option yylineno


%x M_COMMENT
%x S_COMMENT

%x C_STRING

/* ~~~~~~~~~~~~~~~~~~~~~~~ */

%%

{SINGLE_COMMENT}            { BEGIN(S_COMMENT); }
{MULTI_COMMENT}             {
                                comment = malloc(sizeof(int));
                                comment_size++;
                                comment[comment_size - 1] = yylineno;

                                BEGIN(M_COMMENT);
                                counter++;
                            }
{CLOSE_COMMENT}             {
                                fprintf(stderr,"Unnecessary comment closing!\n");
                                return 0;
                            }
<M_COMMENT>{CLOSE_COMMENT}  {
                                counter--;
                                if(counter == 0) {
                                    char comment_start[12];
                                    sprintf(comment_start, "%d", comment[comment_size - 1]); // With sprintf I simply: int to char*
                                    char comment_end[12];
                                    sprintf(comment_end, "%d", yylineno);
                                    comment_size--;
                                    free(comment);

                                    BEGIN(INITIAL);
                                }           
                                else{
                                    char comment_start[12];
                                    sprintf(comment_start, "%d", comment[comment_size - 1]);
                                    char comment_end[12];
                                    sprintf(comment_end, "%d", yylineno);
                                    comment_size--;
                                }
                            }
<M_COMMENT>{MULTI_COMMENT}  {
                                comment_size++;
                                comment = realloc(comment, sizeof(int)*comment_size);
                                comment[comment_size - 1] = yylineno;

                                counter++;
                             }
<S_COMMENT>{NEW_LINE}       { BEGIN(INITIAL); }

<M_COMMENT><<EOF>>          {
                                if(counter!=0)fprintf(stderr,"Error: Uncompleted comment!\n");
                                BEGIN(INITIAL);

                            }
<M_COMMENT>{NEW_LINE}       {}
<M_COMMENT>{SPACE}          {}
<M_COMMENT>{TAB}            {}
<M_COMMENT>{CARRIAGE}       {}
<M_COMMENT>{OTHER}          {}
<S_COMMENT>{OTHER}          {}

{STRING_BEGIN}              {
                                BEGIN(C_STRING);
                                size = 0;
                                string = malloc(sizeof(char));
                            }

<C_STRING>{ESC}               {
                                size++;
                                string = realloc(string,size);

                                char c = input();
                                if(c == 't')
                                    string[size - 1] = '\t';
                                else if(c == 'n')
                                    string[size - 1] = '\n';
                                else if(c == '\\')
                                    string[size - 1] = '\\';
                                else if(c == '\"')
                                    string[size - 1] = '"';
                                else
                                    fprintf(stdout,"Error: Illegal escaped sequence!\n");

                            }
<C_STRING>{STRING_END}        {
                                size++;
                                string = realloc(string,size);
                                string[size - 1] ='\0';

                                //add_element(yylineno, string, STRING);

                                //free(string);
                                BEGIN(INITIAL);

                                yylval.strVal = strdup(string);
                                return STRING;
                            }
<C_STRING><<EOF>>             {
                                fprintf(stdout,"Error: Uncompleted string!\n");
                                BEGIN(INITIAL);
                            }
<C_STRING>{OTHER}             {
                                size++;
                                string = realloc(string,size);

                                string[size -1] = *yytext;
                            }


{REAL_CONST}    { /*add for double later*/ return NUMBER; }
{INT_CONST}     { yylval.intVal = atoi(yytext); return NUMBER;}

{IF}            { return IF; }
{ELSE}          { return ELSE; }
{WHILE}         { return WHILE; }
{FOR}           { return FOR;}
{FUNCTION}      { return FUNCTION; }
{RETURN}        { return RETURN; }
{BREAK}         { return BREAK; }
{CONTINUE}      { return CONTINUE; }
{AND}           { return AND; }
{NOT}           { return NOT; }
{OR}            { return OR; }
{LOCAL}         { return LOCAL; }
{TRUE}          { return TRUE; }
{FALSE}         { return FALSE; }
{NIL}           { return NIL; }

{ASSIGN}        { return ASSIGN; }
{PRODUCT}       { return PRODUCT; }
{OBELUS}        { return OBELUS; }
{MOD}           { return MOD; }
{EQUAL}         { return EQUAL; }
{NOT_EQUAL}     { return NOT_EQUAL; }
{PLUS_PLUS}     { return PLUS_PLUS; }
{MINUS_MINUS}   { return MINUS_MINUS; }
{PLUS}          { return PLUS; }
{MINUS}         { return MINUS; }
{GREATER}       { return GREATER; }
{GREATER_EQUAL} { return GREATER_EQUAL; }
{LESS}          { return LESS; }
{LESS_EQUAL}    { return LESS_EQUAL; }

{IDENTIFIER}        { yylval.strVal = strdup(yytext); return ID; }
{SEMICOLON}         { return SEMICOLON; }
{COLON}             { return COLON; }
{DOUBLE_COLON}      { return DOUBLE_COLON; }
{CURLY_BRACKET_R}   {return CURLY_BRACKET_R;}
{CURLY_BRACKET_L}   {return CURLY_BRACKET_L;}
{BRACKET_L}         { return BRACKET_L; }
{BRACKET_R}         { return BRACKET_R; }
{PARENTHESIS_R}     { return PARENTHESIS_R; }
{PARENTHESIS_L}     { return PARENTHESIS_L; }
{DOT}               { return DOT; }
{DOUBLE_DOT}        { return DOUBLE_DOT; }
{COMA}              { return COMA; }

{NEW_LINE}          {}
{SPACE}             {}
{TAB}               {}
{CARRIAGE}          {}
<<EOF>>             {printf(" EOF \n"); return 0;}
{OTHER}             { printf("No rule for %s\n", yytext);  }

