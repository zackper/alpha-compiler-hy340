/* PHASE 1
* Myrsini Gkolemi 3929
* Zacharias Pervolarakis 4027
*/

#include <stdio.h>
#include <stdbool.h>
#include "int_stack.h"
#include "scope_space.h"
#include "scope_list.h"
#include "quad.h"
#include "symbol_table.h"
#include "expr_vector.h"

//#include "expr.h"


#define STANDARD_TYPE   0
#define GLOBAL_TYPE     1
#define LOCAL_TYPE      2
#define FORMAL_TYPE     3
#define FUNCDEF_TYPE    4
#define FUNCCALL_TYPE   5
#define RETURN_TYPE     6
#define FUNC_RULE       8

char* lib_func_names[12] = {"print","input","objectmemberkeys","objectotalmembers","objectcopy", "argument" ,"totalarguments","typeof","strtonum","sqrt","cos","sin"};

//~~~~~~~~~~~~
//Declarations:
extern int_stack *function_scope;
extern int_stack *space_scope;
extern int_stack *loop_scope;
char *generate_func_name();
ST_Entry *insert_to_lists(char *id, int entry_scope, int line, enum Symbol_t type);

//~~~~~~~~~~~~
//Main functions:
//~~~
void standard_handler(ST_Entry *entry, int line, char *rule)
{
    assert(entry);
    assert(rule);
    char *id = get_entry_name(entry);
    int entry_scope = get_entry_scope(entry);

    //~~~~~~~~~
    if(is_var(entry) && function_scope->size > 0 && function_scope->top > entry_scope && entry_scope > 0)
    {
        a_yyerror("Variable cannot be accessed from interior scope.\n\n", rule, line);
        return;
    }
    //~~~~~~~~~
}
void global_handler(ST_Entry *entry, int line, char *rule)
{
    assert(rule);
    //~~~~~~~~~
    if(entry == NULL)
        a_yyerror("Accesing global variable failed. Variable does not exist.",rule,line);
    //~~~~~~~~~
}
int local_handler(char *id, int entry_scope, int line, char *rule)
{
    assert(id);
    assert(rule);
    //~~~~~~~~~
    if( entry_scope > 0 && is_library_function(id))
    {
        a_yyerror("Local variable trying to overshadow library function.",rule,line);
        return 0;
    };
    //~~~~~~~~~

    return 1;
}
void formal_handler(ST_Entry *entry, int line, char *rule)
{
    assert(entry);
    assert(rule);
    char *id = get_entry_name(entry);
    //~~~~~~~~~
    a_yyerror("Redefinition error.", rule, line);
    //~~~~~~~~~
}
void funcdef_handler(ST_Entry *entry, int line, char *rule)
{
    assert(entry);
    assert(rule);
    char *id = get_entry_name(entry);
    //~~~~~~~~~
    if(is_library_function(id))
    {
        a_yyerror("Function cannot be defined,already library function name.",rule,line);
        return;
    };
    //~~~~~~~~~
    if(is_var(entry))
        a_yyerror("Function cannot be defined,identical name variable defined in this scope.",rule,line);
    else
        a_yyerror("Function cannot be defined,identical name function defined in this scope.",rule,line);
    //~~~~~~~~~
}

ST_Entry *id_handler(char* id, char* rule, int entry_scope, int type, int line)
{
    assert(entry_scope >= 0);
    printf("Id_handler: id: %s, entry_scope: %d, line: %d, type: %d\n", id, entry_scope, line, type);

    ST_Entry *entry = NULL;
    switch(type){
        case STANDARD_TYPE:
            entry = st_standard_look_up(id, entry_scope);
            if(entry == NULL)
            {
                //~~~~~~~ For Scope Spaces
                if(is_empty_stack(space_scope))
                    increment_programVarOffset();
                else
                {
                    printf("INCREMENT LOCAL OFFSET\n");
                    increment_functionLocalOffset(); 
                }

                //~~~~~~~ Insert into lists
                enum Symbol_t var_type;
                if(entry_scope == 0){
                    var_type = GLOBAL_VAR;
                }
                else
                    var_type = LOCAL_VAR;

                ST_Entry *new_entry = insert_to_lists(id, entry_scope, line, var_type);
                return new_entry;
            }
            else
                standard_handler(entry, line, rule);

            break;
        case GLOBAL_TYPE:
            entry = st_scope_look_up(id, 0);
            global_handler(entry, line, rule);
            break;
        case LOCAL_TYPE:
            //~~~~~~~~~
            if(local_handler(id, entry_scope, line, rule) == 0) //Only handler getting called before look up.
                return; 
            //~~~~~~~
            entry = st_scope_look_up(id, entry_scope);
            if(entry == NULL)
            {
                //~~~~~~~ For Scope Spaces
                if(is_empty_stack(space_scope))
                    increment_programVarOffset();
                else
                {
                    printf("INCREMENT LOCAL OFFSET\n");
                    increment_functionLocalOffset(); 
                }
                //~~~~~~~ Insert to lists
                if(entry_scope == 0)
                    entry = insert_to_lists(id, 0, line, GLOBAL_VAR);
                else
                    entry = insert_to_lists(id, entry_scope, line, LOCAL_VAR);
            }
            //else use that variable.

            break;
        case FORMAL_TYPE:
            if(is_library_function(id)){
                a_yyerror("Formal variable trying to overshadow library function.",rule,line);
                return NULL;
            }

            entry = st_scope_look_up(id, entry_scope);
            if(entry == NULL)
                entry = insert_to_lists(id, entry_scope, line, FORMAL_VAR);
            else
                formal_handler(entry, line, rule);

            break;
        case FUNCDEF_TYPE:
            if(id != NULL && is_library_function(id)){
                a_yyerror("User function trying to overshadow library function.",rule,line);
                return NULL;
            }

            if(id == NULL)
                id = generate_func_name(); //No reason to look up, must be unique
            else {
                entry = st_scope_look_up(id, entry_scope);
                if(entry != NULL)
                    printf("Entry found\n");
            }
            assert(id != NULL);
            if(entry == NULL)
                entry = insert_to_lists(id, entry_scope, line, USER_FUNC);
            else
                funcdef_handler(entry, line, rule);
            break;
        case RETURN_TYPE:
            if(entry_scope == 0)
                a_yyerror("Return is not allowed in scope 0.",rule,line);
            break;
        default:
            assert(NULL);
            break;

    }

    return entry;
}
//~~~

void hide_scope(int entry_scope)
{
    assert(entry_scope >= 0);
    if(entry_scope == 0)
        return;
    if(entry_scope > scope_lists->size)
        return;

    ST_Entry *iterator;
    iterator = scope_lists->scope_d_array[scope];

    while(iterator != NULL){
        iterator->isActive = 0;
        iterator = iterator->scope_next;
    }
}

//~~~~~~~~~~~~
//Misc functions;

void check_loop_state(char *rule, int line)
{
    if(loop_scope->size == 0){
        a_yyerror("Wrong use of loop-control command.", rule, line);
    }
    else if(function_scope->top > loop_scope->top){
        a_yyerror("Function inside a loop cant use loop-control commands unless it has a loop.", rule, line);
    }
}

ST_Entry *insert_to_lists(char *id, int entry_scope, int line, enum Symbol_t type)
{
    assert(id);
    assert(entry_scope >= 0);

    ST_Entry *entry;
    entry = create_var_entry(id, entry_scope, line, type);
    st_insert_entry(entry);
    entry_scope = get_entry_scope(entry);
   
    
    insert_scope_lists(scope_lists, entry);

    return entry;
}

int generated_func_number = 0;
char *generate_func_name()
{
    char *generated_id = malloc(sizeof(char)*12);
    sprintf(generated_id, "%d", generated_func_number);
    int len = strlen(generated_id);
    generated_id[len] = '\0';

    generated_func_number++;
    return generated_id;
}

int is_library_function(char* function_name)
{
    assert(function_name);
    int i;
    int check = 0;
    int size = sizeof(lib_func_names)/sizeof(lib_func_names[0]);
    for(i = 0 ; i<size ;i++ )
    {
        if(strcmp(function_name,lib_func_names[i])==0)
            check = 1 ;
    }
    return check;
}

void check_math_term(expr* term, char* rule, int line)
{
    if(term != NULL && term->sym == NULL) //so it wont stop: x = 1;
        return;

    //TODO: Fix this when symbol_types are corrected;
    //if(!is_var(term->sym))
    //  a_yyerror("Function value cannot be altered.", rule, line);
}

//~~~~~~~~~~

void check_assign(/*TODO*/)
{

}

void check_arith(expr* e , char* context, int line)
{
    assert(e);
    assert(context);
    if(e->type == constbool_e||
        e->type == constring_e ||
        e->type == nil_e ||
        e->type == newtable_e ||
        e->type == programfunc_e ||
        e->type == libraryfunc_e ||
        e->type == boolexpr_e)
        {
            a_yyerror("Illegal expr used!", context, line);
        }
}

void check_bool(expr *e, char* context, int line)
{
    assert(e);
    assert(context);
    if(e->type == constnum_e ||
        e->type == constring_e ||
        e->type == nil_e ||
        e->type == newtable_e ||
        e->type == programfunc_e ||
        e->type == libraryfunc_e ||
        e->type == arithexpr_e)
        {
            a_yyerror("Illegal expr used!", context, line);
        }
}

//~~~~~~~~~~

void insert_libfunc()
{
    int i;
    ST_Entry* lib_entry;
    int size = sizeof(lib_func_names)/sizeof(lib_func_names[0]);
    for(i = 0 ; i<size ;i++ )
    {
        lib_entry = create_func_entry(lib_func_names[i],0,0,LIB_FUNC);
        st_insert_entry(lib_entry);
        insert_scope_lists(scope_lists,lib_entry);
    }
}

//~~~~~~~~~~